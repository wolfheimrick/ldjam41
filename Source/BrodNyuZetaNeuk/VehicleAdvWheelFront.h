// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "VehicleWheel.h"
#include "VehicleAdvWheelFront.generated.h"

UCLASS()
class UVehicleAdvWheelFront : public UVehicleWheel
{
	GENERATED_BODY()

public:
	UVehicleAdvWheelFront();
};



