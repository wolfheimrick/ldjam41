#include "MyPawn.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "Runtime/Engine/Classes/Components/InputComponent.h"

// Sets default values
AMyPawn::AMyPawn()
{
	PrimaryActorTick.bCanEverTick = true;
	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VVroum"));
	static ConstructorHelpers::FObjectFinder<UStaticMesh> UltraVroumAsset(TEXT("StaticMesh'/Game/MainVehicule/MainVehiculev2.MainVehiculev2'"));
	mesh->SetStaticMesh(UltraVroumAsset.Object);
	SetRootComponent(mesh);
	mesh->SetSimulatePhysics(true);
}

// Called when the game starts or when spawned
void AMyPawn::BeginPlay() {
	Super::BeginPlay();
}

// Called every frame
void AMyPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	mesh->AddForce(GetActorUpVector() * mesh->GetBodyInstance()->GetBodyMass() * -10000);
}

// Called to bind functionality to input
void AMyPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveForward", this, &AMyPawn::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AMyPawn::MoveRight);
	PlayerInputComponent->BindAxis("LookUp");
	PlayerInputComponent->BindAxis("LookRight");
}

void AMyPawn::MoveForward(float Val) {
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::SanitizeFloat(Val));
	mesh->AddForce(GetActorRightVector() * mesh->GetBodyInstance()->GetBodyMass() * 20000 * Val);
}

void AMyPawn::MoveRight(float Val) {
	AddActorLocalRotation(FRotator(0.0, 1.0 * Val, 0.0));
}