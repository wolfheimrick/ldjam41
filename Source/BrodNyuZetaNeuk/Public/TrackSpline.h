#pragma once

#include "CoreMinimal.h"
#include "Components/SplineComponent.h"
#include "Components/SplineMeshComponent.h"
#include "GameFramework/Actor.h"
#include "TrackSpline.generated.h"

USTRUCT(BlueprintType)
struct FSplineData {
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite) float width;
	UPROPERTY(EditAnywhere, BlueprintReadWrite) float thickness;
	UPROPERTY(EditAnywhere, BlueprintReadWrite) float bank;

	FSplineData() {
		width = 1.0f;
		thickness = 1.0f;
		bank = 0.0f;
	}
};

UCLASS() class BRODNYUZETANEUK_API ATrackSpline : public AActor {
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly) class USplineComponent*		SplineComponent;
	UPROPERTY(EditAnywhere, BlueprintReadOnly) TArray<FSplineData>			SplineDatas;
	UPROPERTY(EditAnywhere, BlueprintReadOnly) bool							bLoop;
	UPROPERTY(EditAnywhere, BlueprintReadOnly) UStaticMesh*					Mesh;

private:
	int			numberOfPoints;

public:	
	ATrackSpline();
	virtual void OnConstruction(const FTransform& transform) override;
	virtual void Tick(float DeltaTime) override;


protected:
	virtual void BeginPlay() override;

private:
	void NewSplineElement(int index, UStaticMesh* elementMesh);
};